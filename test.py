import requests
import unittest
import sys

url = str(sys.argv.pop(1))
response = requests.get(url)

#expected for staging deployment
expected_status_code = 200
#expected result for data1
#data_1 = {'age':'31', 'id':'1', 'name':'Peter'}

#test class
class Test(unittest.TestCase):
    def test_response_status_code(self):
        self.assertEqual(response.status_code, expected_status_code)
    #def test_data1(self):
     #   self.assertEqual(response.json(), data_1)

if __name__ == '__main__':
    unittest.main()
