# Example for Software Development Lifecycle

Shows how a development cycle could look like using CI/CD.

## Overview

![get f'd](https://i.imgur.com/RunJRhI.png)

## Concept

### Stages

```yml
stages:
 - test
 - staging
 - production
 #- monitor
```

**TEST**
* test the code in test environment (docker container)

**STAGING**
* deploy to staging environment to see if deploys correctly

**PRODUCTION**
* deploy to production environment

**MONITOR**
* periodically test the application

### Pipeline workflow

![i double dare you](https://i.imgur.com/pQ0grFv.png)


On commit of code change, the program is tested within a docker container.
```yml 
image:python:3-alpine
```

If successful, the program is deployed and tested on staging server.
On success, new code is pushed to master branch for production.
 
```yml
 script:
   #deploy to staging
   - dpl --provider=heroku --app=$APP_NAME_STAGING --api-key=$HEROKU_API_KEY
   ...
   #test staging deployment
   - python test.py ${APP_TEST_STAGING}
   #commit to master
   - git commit --allow-empty -m "Staging succeeded for ${CI_COMMIT_SHA}"
   - git push -f origin HEAD:master
```

If successful, the program is deployed from master to production server.
```yml
script:
   - dpl --provider=heroku --app=$APP_NAME --api-key=$HEROKU_API_KEY
```

# TODO

Periodically check the production deployment

Harden test code against unforeseen errors (retries, wait times, error handling, etc.)
