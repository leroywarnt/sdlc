#!/usr/bin/env python

#test pipeline 2

import os
from flask import Flask, json, redirect, url_for, send_from_directory
import xml.etree.ElementTree as ET

tree = ET.parse('data.xml')
root = tree.getroot()

app = Flask(__name__)

@app.route('/')
def index():
    return redirect(url_for('get_data'))

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path),'favicon.ico')

@app.route('/data')
def get_data():
    response = 'data:<br /><br />';
    for child in root:
            print('child', child.tag, child.attrib)
            response += '{}x {} ({}) <br />'.format(child.attrib['quantity'], child.attrib['name'], child.attrib['id'])
    response += '<br />';
    return response

@app.route('/data/<datapoint>')
def get_id(datapoint):
    for child in root:
	    if child.attrib['id'] == datapoint:
		    response = app.response_class(
			    response = json.dumps(child.attrib),
				mimetype = 'application/json')
		    return response

if __name__ == '__main__':
    #messing with ports is pointless
    port = os.getenv("PORT", 5000)
    app.run(host='0.0.0.0', port=int(port))
